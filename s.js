function worker(val, arg) {
  let iterator = typeof val === "function" ? val() : val;

  return Promise.resolve().then(() => {
    const a = iterator.next(arg);

    if (a.done) {
      return a.value;
    }

    return new Promise(resolve => {
      if (typeof a.value === "function") {
        resolve(a.value());
      }
      resolve(a.value);
    })
      .then(value => {
        if (typeof value.next === "function") {
          return Promise.resolve(worker(value)).then(val =>
            worker(iterator, val)
          );
        }
        return Promise.resolve(value).then(val => worker(iterator, val));
      })
      .catch(() => {
        return Promise.resolve(a.value).then(val => worker(iterator, val));
      });
  });
}
